#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <stdint.h>
#include <highlighter.h>
#include <qstack.h>
#include <token_grammar.h>
#include <QQueue>
#include <pair_ref.h>
#include <QHash>
#include "unordered_map"
#define TER 101
#define NTE 102
#define EMPTY "$"
#define EPSILON "ε"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    using elements_p = QHash<QString, int>;
    using right = QPair<elements_p, elements_p>;

    int relateChar(QChar c);
private slots:
    void on_actionCheck_Synthax_triggered();

    void on_actionEpsilon_triggered();

private:
    Ui::MainWindow *ui;
    Highlighter highlighter;
    int non_terminal_counter;
    int terminal_counter;
    int current_rule;

    uint8_t stateMachine[6][10]{
        {TER,1  ,3  ,2  ,TER,103,104,TER,0  ,4  },
        {TER,TER,TER,TER,100,TER,TER,TER,TER,TER},
        {2  ,NTE,NTE,2  ,NTE,NTE,NTE,NTE,NTE,2  },
        {TER,TER,3  ,TER,TER,TER,TER,TER,TER,3  },
        {TER,TER,TER,TER,TER,TER,TER,TER,TER,4  },
        {TER,TER,TER,TER,TER,TER,TER,5  ,TER,TER}
    };

    QStack<uint16_t> pila;
    uint8_t predict[5][5]{
      {200,200,0  ,1  ,200},
      {201,2  ,3  ,201,201},
      {202,4  ,4  ,202,5  },
      {203,203,203,203,6  },
      {205,205,8  ,1  ,7  }
    };


    using axis = QPair<int,int>;
    QHash<QString, int> terminal_map;
    QHash<QString, int> non_terminal_map;
    QHash<QString, right> empty_set_follows;
    QHash<QString, right> empty_set_first;
    QHash<axis, int> predict_table;

    using rule = QPair<QString, QList<QString>>;
    QList<rule> rules;

    void aniadirProducciones(int axis);
    void calcFollowSet();
    void printFollowSet(QString &text);
    bool isTerminal(QString token);
    void semanthicActions(int accion, QPair<QString, uint8_t> &token);
    void iterateOverRules(QPair<QList<QString>, int> right, QString &firstSet, QString &left);
    bool containsLambda(QString &non_terminal);
    void findFollowNonTerminal(QString non_terminal, int number_production, QString &first_set);
    void calcFollowSetR(QString non_terminal, QString &follow_string);
    int findTerminalIndex(QString element);
    int findNonTerminalIndex(QString element);
    void findFirstNonTerminal(right root, right target, int number_production);
    void printPredictTable(QString &text);
    void insertIntoPredictTable(QString nonterminal, QString terminal, int production);
    void findFirstFromNextNonTerminal(right *root, right *target, int &production);
    void calcFirstSet();
    void printFirstSet(QString &text);
    void findFirstNonTerminal(right *root, right *target, int number_production);
    void findElementsFromNextNonTerminal(right *root, right *target, int &production);
    void findFollowsFromBeta(right *root, right *target, int &production);
    void constructPredictTable();
    void printProductions(QString &text);
    int getIndexFromElement(QString &element);
};
#endif // MAINWINDOW_H
