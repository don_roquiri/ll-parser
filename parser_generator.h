#pragma once
#include "string"
#include "unordered_map"
#include "stack"
#include "string"
#include "list"
#include "vector"
#include "iostream"
#define TER 101
#define NTE 102
#define EMPTY "$"
#define EPSILON "ε"

struct pairhash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
  }
};


using namespace std;
class parser_generator{
public:
    parser_generator(string& text);
    void calcFollowSet();
    void calcFirstSet();
private:

    int non_terminal_counter;
    int terminal_counter;
    int semanthic_action_counter;

    int current_rule;

    struct Token {
        string lexem;
        int gramem;
    } token;

    string text;

    using elements_p = unordered_map<string, pair<int, int>>;
    using right = pair<elements_p, elements_p>;
    using axis = pair<int,int>;

    unordered_map<string, int> terminal_map;
    unordered_map<string, int> non_terminal_map;
    unordered_map<string, int> semanthic_actions_map;
    unordered_map<string, right> follow_set;
    unordered_map<string, right> first_set;
    unordered_map<axis, int, pairhash> predict_table_;

    using rule = pair<string, vector<string>>;
    vector<rule> rules;

    const unordered_map<std::pair<int, int>, int, pairhash> predict_table {
        {{7, 1010}, 14}, {{7, 1011}, 14}, {{1, 1006}, 1}, {{5, 1007}, 7},
        {{7, 1009}, 14}, {{5, 1005}, 8}, {{1, 1002}, 0}, {{5, 1000}, 8},
        {{1, 1000}, 2}, {{6, 1010}, 11}, {{6, 1011}, 12}, {{4, 1006}, 6},
        {{6, 1009}, 10}, {{1, 1012}, 2}, {{7, 1007}, 14}, {{3, 1002}, 5},
        {{7, 1000}, 13}, {{6, 1007}, 9}, {{2, 1000}, 3},
    };;


    vector<vector <int>> state_machine {
        {1,	2,	1,	3,	1,	0,	0,	0,	0,	4,	1,	1,	1004,	1005,	6},
        {1,	1,	1,	1,	1,	1003,	1003,	1003,	1003,	1,	1,	1,	1,	1,	1},
        {1,	2,	2,	1007,	1007,	1007,	1007,	1007,	1007,	1007,	2,	1007,	1007,	1007,	1007},
        {1,	1,	1,	1,	1008,	1,	1,	1,	1003,	1,	1,	1,	1,	1,	1},
        {5,	5,	500,	500,	500,	1003,	1003,	1003,	1003,	500,	500,	500,	500,	500,	500},
        {5,	5,	5,	501,	1009,	501,	501,	501,	501,	501,	5,	501,	501, 501,	501},
        {1,	1,	1,	1,	1,	1010,	1010,	1010,	1010,	1,	1,	1,	1,	1,	1}
    };

    list<int> executionStack;

    void fast();
    int relateChar(char &car);
    void parser();
    void attatch(int &axis);
    void llParser(Token &token);
    void semanthicActions(int &action);
    void iterateOverRules();
    bool isTerminal(string &element);
    bool isNonTerminal(string &element);

    bool containsEpsilon(right *element);
    void findFirstNonTerminal(right *root, right *target, pair<int, int> number_production);
    void findFollowFromBeta(right *root, right *target);
    void constructPredictTable();
    int getNonTerminalIndex(string element);
    int getTerminalIndex(string element);
    void compile(string &text);
};
