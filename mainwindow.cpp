#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "highlighter.h"
#include "qdebug.h"
#include "QMessageBox"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    highlighter.setDocument(ui->txtGlc->document());
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionCheck_Synthax_triggered()
{
    terminal_counter = 1000;
    current_rule = non_terminal_counter = 1;
    QString grammar = ui->txtGlc->toPlainText();
    grammar += " \n\x0 ";
    int edo = 0;
    int i = 0;
    pila.clear();
    empty_set_first.clear();
    empty_set_follows.clear();
    predict_table.clear();
    rules.clear();
    terminal_map.clear();
    non_terminal_map.clear();
    pila.append(103);
    pila.append(0);
    QString left_pivot;
    QChar car;
    QPair<QString, uint8_t> token;

    while (i < grammar.size() + 1) {
        while(edo <= 3){
            car = grammar[i++];
            edo = stateMachine[edo][relateChar(car)];
            if(edo != 0)
                token.first += car;
        }
        token.second = edo;
        if(edo == 101 || edo == 102)
            if(token.first.size() > 1){
                token.first.chop(1);
                i--;
            }

        while(true){
            auto top = pila.top();
            pila.pop();
            if(top <= 5){
                uint8_t axis = predict[top][token.second - 100];
                if(axis < 200){
                    qDebug() << "Intersección: " << axis;
                    aniadirProducciones(axis);
                }else{
                    QMessageBox::warning(this,"Error","Error la sintaxis no coincide" + QString::number(axis));
                    return;
                }
            }else{
                if(top == token.second){
                    break;
                }else{
                    if(top >= 2000){
                        semanthicActions(top, token);
                    }else{
                        QMessageBox::warning(this,"Error no match", "Error la sintaxis no coincide\n"
                                             + QString::number(top) + " : " + QString::number(token.second));
                        return;
                    }
                }
            }
        }
        edo = 0;
        token.first.clear();
    }
}

inline void MainWindow::semanthicActions(int accion, QPair<QString, uint8_t>& token){
    switch (accion) {
    case 2000:{//Non terminal Left Side
        QString leftside = token.first;
        QList<QString> right_rule;
        rules.append({leftside, {}});
        if(!non_terminal_map.contains(leftside)){
            non_terminal_map.insert(leftside, non_terminal_counter++);
            empty_set_first.insert(leftside, {});
            empty_set_follows.insert(leftside, {});
        }
        break;
    }
    case 2001:{//Terminal
        rules.last().second.append(token.first);
        if(!terminal_map.contains(token.first))
            terminal_map.insert(token.first, terminal_counter++);
        break;
    }
    case 2002:{//Non terminal Right Side
        rules.last().second.append(token.first);
        break;
    }
    case 2003:{//eof

        QString first_follow_string = "#include \"iostream\"\n"
                                      "#include \"unordered_map\"\n"
                                      "#include \"stack\"\n"
                                      "#include \"string\"\n"
                                      "using namespace std;\n"
                                      "stack<int> executionStack;\n"
                                      "struct Token{int gramema; string lexem;};\n";

        terminal_map.insert("$", terminal_counter);

        calcFirstSet();
        calcFollowSet();
        constructPredictTable();

        printFirstSet(first_follow_string);
        printFollowSet(first_follow_string);
        printPredictTable(first_follow_string);
        printProductions(first_follow_string);

        ui->res->setPlainText(first_follow_string);
        break;
    }
    }

}

void MainWindow::printPredictTable(QString& text){
    text += "\n//Predict Table"
            "\nstd::unordered_map<std::pair<int, int>, int> predict_table {\n";
    int i = 1;
    for(auto iterator = QHashIterator<axis, int>(predict_table);iterator.hasNext();){
        auto current = iterator.next();
        text += "{{" + QString::number(current.key().first) + ", " + QString::number(current.key().second) + "}, " +
                QString::number(current.value()) + "}, ";
        if(i++ > 3){
            i = 1;
            text += "\n";
        }
    }
    text += "\n};\n";
}

void MainWindow::calcFirstSet(){
    for (int i = 0; i < rules.size(); i++) {
        auto debug = qDebug();
        debug << rules.at(i).first << "{";
        std::for_each(rules.at(i).second.begin(), rules.at(i).second.end(), [&debug](QString element){
            debug << element << ", ";
        });
        debug << "}";
    }

    int current_rule = 0;

    for(auto rule : rules){
        auto left = rule.first;
        auto right = rule.second;
        auto finded = empty_set_first.find(left);
        if(isTerminal(right[0])){
            finded.value().first.insert(right[0], current_rule++);
        }else{
            finded.value().second.insert(right[0], current_rule++);
        }
    }

    auto iterator = QMutableHashIterator<QString,right>(empty_set_first);
    while(iterator.hasNext()){
        auto current = iterator.next();
        right* root = &current.value();
        for(auto elements = QMutableHashIterator<QString, int>(root->second); elements.hasNext();){
            auto element = elements.next();
            elements.remove();
            right* target = &empty_set_first.find(element.key()).value();
            if(target != nullptr)
                findFirstNonTerminal(root, target, element.value());
        }
    }
}

inline void MainWindow::findFirstNonTerminal(right* root, right* target, int number_production){
    if(root == target) return;
    bool root_contain_epsilon = root->first.contains(EPSILON);
    int empty_root_rule;

    if(root_contain_epsilon){empty_root_rule = root->first.find(EPSILON).value();}

    for(auto i = QMutableHashIterator<QString,int>(target->second); i.hasNext();){
        auto element = i.next(); i.remove();
        auto* new_target = &empty_set_first.find(element.key()).value();
        if(new_target == nullptr) return;
        findFirstNonTerminal(target, new_target, element.value());
    }

    for(auto i = QMutableHashIterator<QString,int>(target->first); i.hasNext();){
        auto current = i.next();
        root->first.insert(current.key(), number_production);
    }

    //Si la produccion deriva vacio buscar a la derecha los first de el no terminal

    int i = 1;
    bool has_empty = target->first.contains(EPSILON);
        while (has_empty && i < rules[number_production].second.size()) {
        auto& new_target = rules[number_production].second[i];

        if(isTerminal(new_target)){
            if(!root_contain_epsilon) root->first.remove(EPSILON);
            root->first.insert(new_target, number_production);
            break;
        } else {
            i++;
            //agregar los no terminales
            right* target = &empty_set_first.find(new_target).value();
            if(root == target){root->first.remove(EPSILON); return;}

            for(auto i = QMutableHashIterator<QString,int>(target->second); i.hasNext();){
                auto element = i.next(); i.remove();
                auto* new_target = &empty_set_first.find(element.key()).value();
                if(new_target == nullptr) return;
                findFirstNonTerminal(target, new_target, element.value());
            }

            for(auto i = QMutableHashIterator<QString,int>(target->first); i.hasNext();){
                auto current = i.next();
                root->first.insert(current.key(), number_production);
            }
            if(!target->first.contains(EPSILON)){
                root->first.remove(EPSILON);
                break;
            }
        }
    }
    if(root_contain_epsilon){
        root->first.insert(EPSILON, empty_root_rule);
    }
}

inline void MainWindow::insertIntoPredictTable(QString nonterminal, QString terminal, int production){
    if(!predict_table.contains({findNonTerminalIndex(nonterminal), terminal_map.find(terminal).value()}))
        predict_table.insert({findNonTerminalIndex(nonterminal), terminal_map.find(terminal).value()}, production);
}

inline int MainWindow::findNonTerminalIndex(QString element){
    auto finded = non_terminal_map.find(element);
    if(finded == non_terminal_map.end()){
        non_terminal_map.insert(element, non_terminal_counter);
        throw "Error No hay ningun elemento no terminal llamado asi, dando un valor de comodin";
        return non_terminal_counter++;
    }else{
        return finded.value();
    }
}

inline void MainWindow::calcFollowSet(){
    empty_set_follows.find(rules.first().first).value().first.insert("$",terminal_map.find(EPSILON).value());
    for(rule r : rules){
        auto A = r.first;
        auto pivot = r.second;
        for (int i = 0; i < pivot.size(); i++){
            QString current = pivot[i];
            if(!isTerminal(current)){
                if(i + 1 < pivot.size()) {//A -> alfa B beta
                    auto follow = pivot[i + 1];
                    auto B = empty_set_follows.find(current);
                    if(B != empty_set_follows.end()){
                        if(isTerminal(follow)){
                            B.value().first.insert(follow, -1);
                        }else{
                            auto b = empty_set_first.find(follow);
                            if(b != empty_set_first.end()){
                                for(auto terminal : b.value().first.keys()){
                                    B.value().first.insert(terminal, -1);
                                }
                                if(b.value().first.contains(EPSILON) && current != A){
                                    B.value().first.remove(EPSILON);
                                    B.value().second.insert(A, -1);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    auto iterator = QMutableHashIterator<QString,right>(empty_set_follows);
    while(iterator.hasNext()){
        auto current = iterator.next();
        right* root = &current.value();
        for(auto elements = QMutableHashIterator<QString, int>(root->second); elements.hasNext();){
            auto element = elements.next();
            elements.remove();
            right* target = &empty_set_follows.find(element.key()).value();
            if(target != nullptr)
                findFollowsFromBeta(root, target, element.value());
        }
    }
}

inline void MainWindow::findFollowsFromBeta(right* root, right* target, int& production){
    for(auto iterator = QMutableHashIterator<QString, int>(target->second); iterator.hasNext(); ){
        auto element = iterator.next(); iterator.remove();
        auto* new_target = &empty_set_follows.find(element.key()).value();
        findFollowsFromBeta(target, new_target, production);
    }

    for(auto iterator = QHashIterator<QString, int>(target->first); iterator.hasNext();){
        auto element = iterator.next();
        root->first.insert(element.key(), production);
    }
}

void MainWindow::constructPredictTable(){
    for(auto i = QHashIterator<QString, right>(empty_set_first); i.hasNext();){
        auto set = i.next();
        auto& firsts = set.value().first;
        auto non_terminal = set.key();
        for(auto it = QHashIterator<QString, int>(firsts); it.hasNext();){
            auto first = it.next();
            if(first.key() != EPSILON)
                insertIntoPredictTable(non_terminal, first.key(), first.value());
        }
        if(firsts.contains(EPSILON)){
            int production_to_empty = firsts.find(EPSILON).value();
            auto finded = empty_set_follows.find(non_terminal);
            if(finded != empty_set_follows.end()){
                auto& follows = finded.value();
                for(auto it = QHashIterator<QString, int>(follows.first); it.hasNext();){
                    auto follow = it.next();
                    insertIntoPredictTable(non_terminal, follow.key(), production_to_empty);
                }
            }
        }
    }
}

void MainWindow::printFirstSet(QString& text){
    text += "/*\n";
    text += "Terminals Index\n";
    for(auto iterator = QHashIterator<QString, int>(terminal_map); iterator.hasNext();){
        auto current = iterator.next();
        text += current.key() + " = " + QString::number(current.value()) + "\n";
    }
    text += "Non Terminals Index\n";
    for(auto iterator = QHashIterator<QString, int>(non_terminal_map); iterator.hasNext();){
        auto current = iterator.next();
        text += current.key() + " = " + QString::number(current.value()) + "\n";
    }
    text += "\n--------- First Set ---------\n";
    for(auto iterator = QHashIterator<QString, right>(empty_set_first);iterator.hasNext();){
        auto next = iterator.next();
        text += "First(" + next.key() + "){ ";
        auto keys = next.value().first.keys();
        for(auto terminal : keys){
            text += terminal + ", ";
        }
        if(keys.size() > 0){text.chop(2); text+=" ";}
        text += "}\n";
    }
}

void MainWindow::printFollowSet(QString &text){
    text += "\n--------- Follow Set ---------\n";
    for(auto iterator = QHashIterator<QString, right>(empty_set_follows);iterator.hasNext();){
        auto next = iterator.next();
        text += "Follow(" + next.key() + "){ ";
        auto keys = next.value().first.keys();
        for(auto terminal : keys){
            text += terminal + ", ";
        }
        if(keys.size() > 0){text.chop(2); text+=" ";}
        text += "}\n";
    }
    text += "*/";
}

void MainWindow::printProductions(QString& text){
    text += "inline void attatch(int &axis){\n";
    text += "switch(axis){\n";
    int n_rule = 0;
    for(auto rule : rules){
        if(rule.second[0] == EPSILON){
            text += "\tcase " + QString::number(n_rule++) + ": break;\n";
        }else{
            text += "\tcase " + QString::number(n_rule++) + ": executionStack.emplace(";
            for(int i = rule.second.size() - 1; i >= 1; i--){
                auto& current = rule.second[i];
                text += QString::number(getIndexFromElement(current)) +  ", ";
            }
            text += QString::number(getIndexFromElement(rule.second[0]));
            text += "); break;\n";
        }
    }
    text += "}\n";
    text += "}\n";
    text += "inline void llParser(Token& token){\n";
    text += "\twhile(true){\n"
            "\t\tauto top = executionStack.top();\n"
            "\t\texecutionStack.pop();\n"
            "\t\tif(top <= " + QString::number(non_terminal_counter - 1) + "){\n"
            "\t\t\tauto finded = predict_table.find({top, token.gramema});\n"
            "\t\t\tif(finded == predict_table.end()){\n"
            "\t\t\t\tint error = 700 + top + (token.gramema - 1000);\n"
            "\t\t\t\tstd::cout<<\"Synthax Error! \" << error;\n"
            "\t\t\t\treturn;\n"
            "\t\t\t}else{\n"
            "\t\t\t\tint axis = finded.value();\n"
            "\t\t\t\tattatch(axis);\n"
            "\t\t\t}\n"
            "\t\t}else if(top == token.gramema){\n"
            "\t\t\tbreak;\n"
            "\t\t}else if(top >= 2000){\n"
            "\t\t\tsemanthicActions(top);\n"
            "\t\t}else{\n"
            "\t\t\tstd::cout<<\"Error nomatch\";\n"
            "\t\t\treturn;\n"
            "\t\t}\n"
            "\t}\n";
    text += "}";
}

inline int MainWindow::getIndexFromElement(QString& element){
    auto finded = terminal_map.find(element);
    if(finded == terminal_map.end()){
        return non_terminal_map.value(element);
    }
    return finded.value();
}

inline bool MainWindow::isTerminal(QString token){
    return terminal_map.contains(token);
}

inline bool MainWindow::containsLambda(QString &non_terminal){
    return empty_set_first.find(non_terminal).value().second.contains(EPSILON);
}

inline int MainWindow::relateChar(QChar car){
    if(car >= 'A' && car <= 'Z'){return 3;}
    else if(car >= 'a' && car <= 'z'){return 2;}
    else if(car >= '0' && car <= '9'){return 9;}
    else if(car == '>'){return 4;}
    else if(car == '_'){return 0;}
    else if(car == '-'){return 1;}
    else if(car == '\n'){return 6;}
    else if(car == '\x0'){return 5;}
    else if(car == ' '){return 8;}
    else{return 7;}
}

inline void MainWindow::aniadirProducciones(int axis){
    switch (axis) {
    case 0:{pila.append({0,3,1,100,102,2000}); break;}
    case 1:{pila.append({103, 2003}); break;}
    case 2:{pila.append({2,101,2001}); break;}
    case 3:{pila.append({2,102,2002}); break;}
    case 4:{pila.append({1});break;}
    case 5:{break;}
    case 6:{pila.append({4,104}); break;}
    case 7:{pila.append({3});break;}
    case 8:{break;}
    }
}

void MainWindow::on_actionEpsilon_triggered()
{
    ui->txtGlc->insertPlainText(EPSILON);
}
