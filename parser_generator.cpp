#include "parser_generator.h"
/*
Terminals Index
semanthic = 1009
noterminal = 1007
ε = 1001
} = 1005
parser = 1006
arrow = 1008
endline = 1000
terminal = 1003
| = 1010
lexer = 1002
{ = 1004
$ = 1011
Non Terminals Index
PARSER = 4
RIGHT = 6
START = 1
PARSER2 = 5
RIGHT2 = 7
BLANK = 2
LEXER = 3

--------- First Set ---------
First(PARSER){ parser }
First(RIGHT){ semanthic, noterminal, terminal, | }
First(START){ ε, parser, endline, lexer }
First(PARSER2){ noterminal, ε, endline }
First(RIGHT2){ semanthic, noterminal, endline, |, terminal }
First(BLANK){ ε, endline }
First(LEXER){ lexer }

--------- Follow Set ---------
Follow(PARSER){ }
Follow(RIGHT){ }
Follow(START){ $ }
Follow(PARSER2){ } }
Follow(RIGHT2){ }
Follow(BLANK){ }
Follow(LEXER){ }

START->LEXER
START->PARSER
START->BLANK
BLANK->endline BLANK
BLANK->ε
LEXER->lexer terminal { LEXER2 }
PARSER->parser terminal { PARSER2 }
PARSER2->noterminal arrow RIGHT
PARSER2->BLANK
RIGHT->noterminal RIGHT2
RIGHT->terminal RIGHT2
RIGHT->semanthic RIGHT2
RIGHT->| RIGHT2
RIGHT2->endline
RIGHT2->RIGHT
RIGHT2->| RIGHT
*/
//Predict Table

void parser_generator::compile(string& text){
    int edo;
    for(auto ch = text.begin(); ch != text.end(); ch++){
        edo = 0;
        while(edo <= 6){
            edo = state_machine[edo][relateChar(*ch)];
            token.lexem += *ch;
        }
        token.gramem = edo;
        llParser(token);
    }
}

inline int parser_generator::relateChar(char& ch){
    switch (ch) {
    case 'a'...'z': return 0;
    case 'A'...'Z': return 1;
    case '0'...'9': return 2;
    case '-': return 3;
    case '>': return 4;
    case '\n': return 5;
    case '\t': return 6;
    case '\x0': return 3;
    case ' ': return 8;
    case '<': return 9;
    case '_': return 10;
    case '{': return 12;
    case '}': return 13;
    default: return 11;
    }
}

inline void parser_generator::attatch(int &axis){
    switch(axis){
    case 0:executionStack.insert(executionStack.end(), {3}); break;
    case 1: executionStack.insert(executionStack.end(), {4}); break;
    case 2: executionStack.insert(executionStack.end(), {2}); break;
    case 3: executionStack.insert(executionStack.end(),{2, 1000}); break;
    case 4: break;
    case 5: executionStack.insert(executionStack.end(),{1005, 0, 1004, 1003, 1002}); break;
    case 6: executionStack.insert(executionStack.end(),{1005, 5, 1004, 1003, 1006}); break;
    case 7: executionStack.insert(executionStack.end(),{6, 1008, 1007}); break;
    case 8: executionStack.insert(executionStack.end(),{2}); break;
    case 9: executionStack.insert(executionStack.end(),{7, 1007}); break;
    case 10: executionStack.insert(executionStack.end(),{7, 1003}); break;
    case 11: executionStack.insert(executionStack.end(),{7, 1009}); break;
    case 12: executionStack.insert(executionStack.end(),{7, 1010}); break;
    case 13: executionStack.insert(executionStack.end(),{1000}); break;
    case 14: executionStack.insert(executionStack.end(),{6}); break;
    case 15: executionStack.insert(executionStack.end(),{6, 1010}); break;
    }
}

inline void parser_generator::llParser(Token& token){
    while(true){
        auto top = executionStack.back();
        executionStack.pop_back();
        if(top <= 7){
            auto finded = predict_table.find({top, token.gramem});
            if(finded == predict_table.end()){
                int error = 700 + top + (token.gramem - 1000);
                std::cout << "Synthax Error! " << error;
                return;
            }else{
                int axis = finded->second;
                attatch(axis);
            }
        }else if(top == token.gramem){
            break;
        }else if(top >= 2000){
            semanthicActions(top);
        }else{
            std::cout << "Error nomatch";
            return;
        }
    }
}

inline void parser_generator::semanthicActions(int &action){
    switch(action){
    case 2000:{//nonterminal left side
        string& leftside = token.lexem;
        list<string> right_rule;
        rules.push_back({leftside, {}});
        if(non_terminal_map.find(leftside) == non_terminal_map.end()){
            non_terminal_map[leftside] = non_terminal_counter++;
            vector<int> a;
            first_set[leftside] = {};
            follow_set[leftside] = {};
        }
        break;
    }
    case 2001:{//terminal
        string&terminal = token.lexem;
        rules.back().second.push_back(terminal);
        if(terminal_map.find(terminal) == terminal_map.end()){
            terminal_map[terminal] = terminal_counter++;
        }
        break;
    }
    case 2002:{//nonterminal common
        string& nonterminal = token.lexem;
        rules.back().second.push_back(nonterminal);
        if(non_terminal_map.find(nonterminal) == non_terminal_map.end()){
            non_terminal_map[nonterminal] = non_terminal_counter++;
            first_set[nonterminal] = {};
            follow_set[nonterminal] = {};
        }
        break;
    }
    case 2003:{//or
        rules.push_back({rules.back().first, {}});
        break;
    }
    case 2004:{//semanthic actions
        string& semanthic_name = token.lexem;
        if(semanthic_actions_map.find(semanthic_name) == semanthic_actions_map.end()){
            semanthic_actions_map[semanthic_name] = semanthic_action_counter++;
        }
        break;
    }
    case 2005://eof
        calcFirstSet();
        calcFollowSet();

        break;
    }
}

inline void parser_generator::iterateOverRules(){
    int number_rule = 0;
    for (rule r : rules){
        auto& left = r.first;
        auto& right = r.second;
        bool inserted_first = false;
        for (unsigned int i = 0; i < right.size(); i++){
            auto& current = right[i];
            bool is_non_terminal = isNonTerminal(current);
            if(!inserted_first){
                if(isTerminal(current)){
                    first_set.at(left).first[current] = {number_rule, i};
                    inserted_first = true;
                }else if(is_non_terminal){
                    first_set.at(left).second[current] = {number_rule, i};
                    inserted_first = true;
                }
            }
            if(is_non_terminal){
                unsigned int j = i + 1;
                while(j < right.size()){
                    auto& follow = right[j];
                    if(isTerminal(follow)){
                        follow_set.at(left).first[current] = {number_rule, j};
                        break;
                    }else if(isNonTerminal(follow)){
                        follow_set.at(left).second[current] = {number_rule, j};
                        break;
                    }
                }
            }
        }
        number_rule++;
    }
}

inline void parser_generator::calcFirstSet(){
    for(auto i = first_set.begin(); i != first_set.end(); i++   ){
        auto* root = &i->second;
        for(auto& element : i->second.second){
            auto* target = &first_set.at(element.first);
            i->second.second.erase(element.first);
            findFirstNonTerminal(root, target, element.second);
        }
    }
}

inline void parser_generator::findFirstNonTerminal(right* root, right* target, pair<int, int> number_production){
    if(root == target) return;

    bool root_contain_epsilon = containsEpsilon(root);
    int empty_root_rule;

    if(root_contain_epsilon){
        empty_root_rule = root->first.find(EPSILON)->second.first;
    }

    for(auto i = target->second.begin(); i != target->second.end(); i++){
        auto n_production = i->second;
        auto* new_target = &first_set.at(i->first);
        target->second.erase(i->first);
        findFirstNonTerminal(target, new_target, n_production);
    }

    for(auto i = target->first.begin(); i != target->first.end();){
        root->first[i->first] = number_production;
    }

    //Si la produccion deriva vacio buscar a la derecha los first de el no terminal

    unsigned int i = number_production.second + 1;
    bool has_empty = containsEpsilon(target);

    for (;has_empty && i < rules[number_production.first].second.size(); i++) {
        auto& new_target = rules[number_production.first].second[i];

        if(isTerminal(new_target)){
            if(!root_contain_epsilon) root->first.erase(EPSILON);
            root->first[new_target] = number_production;
            break;
        } else if(isNonTerminal(new_target)) {
            //agregar los no terminales
            right* target = &first_set.at(new_target);

            if(root == target){root->first.erase(EPSILON); return;}

            for(auto it = target->second.begin(); it != target->second.end(); it++){
                auto n_production = it->second;
                auto* new_target = &first_set.at(it->first);
                target->second.erase(it->first);
                findFirstNonTerminal(target, new_target, n_production);
            }

            for(auto it = target->first.begin(); it != target->first.end(); it++){
                root->first[it->first] = number_production;
            }

            if(!containsEpsilon(target)){
                root->first.erase(EPSILON);
                break;
            }
        }
    }

    if(root_contain_epsilon) root->first[EPSILON].first = empty_root_rule;
}

inline void parser_generator::calcFollowSet(){
    for(auto i = follow_set.begin(); i != first_set.end(); i++){

        for(auto& b : i->second.second){
            auto target = b.first;
            auto* first = &first_set.at(target);

            for(auto follows : first->first){
                i->second.first[follows.first] = follows.second;
            }

            if(containsEpsilon(first)){
                auto& A = rules[b.second.first].first;
                i->second.first.erase(EPSILON);
                auto* target = &follow_set.at(A);
                findFollowFromBeta(&i->second, target);
            }
        }
    }
}

//A -> alfa B beta
inline void parser_generator::findFollowFromBeta(right* root, right* target){
    for(auto& b : target->second){
        auto new_target = b.first;
        auto* first = &first_set.at(new_target);

        for(auto first : first->first){
            root->first[first.first] = first.second;
        }

        if(containsEpsilon(first)){
            auto& A = rules[b.second.first].first;
            root->first.erase(EPSILON);
            auto* new_target = &follow_set.at(A);
            findFollowFromBeta(target, new_target);
            for(auto follows : target->first){
                root->first[follows.first] = follows.second;
            }
        }
    }
}

inline bool parser_generator::containsEpsilon(right* element){
    return element->first.find(EPSILON) != element->first.end();
}

inline bool parser_generator::isTerminal(string& element){
    return terminal_map.find(element) != terminal_map.end();
}

inline bool parser_generator::isNonTerminal(string& element){
    return non_terminal_map.find(element) != non_terminal_map.end();
}

inline int parser_generator::getTerminalIndex(string element){
    return terminal_map.at(element);
}

inline int parser_generator::getNonTerminalIndex(string element){
    return non_terminal_map.at(element);
}

inline void parser_generator::constructPredictTable(){
    for(auto i = first_set.begin(); i != first_set.end();){
        auto firsts = &i->second;
        string leftside = i->first;
        for(auto first : firsts->first){
            predict_table_[{getNonTerminalIndex(leftside), getTerminalIndex(first.first)}] = first.second.first;
        }
        auto finded = firsts->first.find(EPSILON);
        if(finded != firsts->first.end()){
            auto* follows = &follow_set.at(leftside);
            int& empty_production_derived = finded->second.first;
            for(auto follow : follows->first){
                predict_table_[{getNonTerminalIndex(leftside), getTerminalIndex(follow.first)}] = empty_production_derived;
            }
        }
    }
}
